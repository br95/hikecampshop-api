BEGIN
INSERT INTO Kategorija (id, naziv) VALUES (1, 'Tents');
INSERT INTO Kategorija (id, naziv) VALUES (2, 'Backpacks');
INSERT INTO Kategorija (id, naziv) VALUES (3, 'Pads & Hammocks');
INSERT INTO Kategorija (id, naziv) VALUES (4, 'Lighting');
INSERT INTO Kategorija (id, naziv) VALUES (5, 'Water');
INSERT INTO Kategorija (id, naziv) VALUES (6, 'Camp Furniture');
INSERT INTO Kategorija (id, naziv) VALUES (7, 'Camp Kitchen');
INSERT INTO Kategorija (id, naziv) VALUES (8, 'Electronics');
INSERT INTO Kategorija (id, naziv) VALUES (9, 'Gadgets & Gear');
INSERT INTO Kategorija (id, naziv) VALUES (10, 'Hiking Footwear');
INSERT INTO Kategorija (id, naziv) VALUES (11, 'Gadgets & Gear');
INSERT INTO Kategorija (id, naziv) VALUES (12, 'Hiking Clothing');
INSERT INTO Kategorija (id, naziv) VALUES (13, 'Accessories');
INSERT INTO Kategorija (id, naziv) VALUES (14, 'Health & Safety');
SELECT setval('KategorijaSequence', 14);
END;

BEGIN
INSERT INTO proizvod (id, cijena, trenutnaZaliha, naziv, opis, pol, porez, slikaimgururl, kategorija_id) VALUES (1,39.93,32,'REI Co-op Trail 25 Pack','The comfortable, durable women’s REI Co-op Trail 25 pack holds plenty of gear—and keeps it well organized—for day hikes, commuting and carry-on travel.','WOMEN',13,'https://i.imgur.com/YdB3d5W.jpg',2);
INSERT INTO proizvod (id, cijena, trenutnaZaliha, naziv, opis, pol, porez, slikaimgururl, kategorija_id) VALUES (2,54.95,13,'REI Co-op Flash 22 Pack','The REI Co-op Flash 22 pack combines ultralight design with trail-smart touches to keep you comfortable and well-supplied on a day hike or on the commute to work.','NEUTRAL',13,'https://i.imgur.com/10VcPXN.jpg',2);
INSERT INTO proizvod (id, cijena, trenutnaZaliha, naziv, opis, pol, porez, slikaimgururl, kategorija_id) VALUES (3,65.00,43,'Osprey Daylite Plus Pack','Take the Osprey Daylite Plus pack on a one-day hike or a plane trip as your carry-on. A padded harness and simple webbing hipbelt make it easy and comfortable to carry.','NEUTRAL',12,'https://i.imgur.com/fjRoBjA.jpg',2);
INSERT INTO proizvod (id, cijena, trenutnaZaliha, naziv, opis, pol, porez, slikaimgururl, kategorija_id) VALUES (4,139.00,4,'REI Co-op Traverse 35 Pack','The REI Co-op Traverse 35 pack holds plenty of gear for overnighters, but its sleek enough to carry as a daypack. And its designed to make every step of your trail time more comfortable','MANS',16,'https://i.imgur.com/qWbIDYM.jpg',2);
INSERT INTO proizvod (id, cijena, trenutnaZaliha, naziv, opis, pol, porez, slikaimgururl, kategorija_id) VALUES (5,139.00,14,'REI Co-op Traverse 35 Pack','Roomy enough to hold plenty of gear for an overnighter yet sleek enough for day hikes, the womens REI Co-op Traverse 35 pack is comfortable to carry, no matter how many miles you log.','WOMEN',11,'https://i.imgur.com/kySIqAu.jpg',2);
SELECT setval('ProizvodSequence', 5);
END; 