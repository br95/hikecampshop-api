package lothar.hcs_services.exception.matcher;
public enum PersistenceExceptionMatcher {
	UNIQUE_CONSTRAINT_VIOLATION("(?s).*Key \\((.*)\\)=\\((.*)\\) already exists(?s).*"),
	NOT_NULL_CONSTRAINT_VIOLATION("(?s).*.*null value in column \"(.*)\" violates not-null constraint.*(?s).*"),
	FOREIGN_KEY_CONSTRAINT_VIOLATION("(?s).*.*insert or update on table \"(.*)\" violates foreign key constraint .*(?s).*");
	private String pattern;

	private PersistenceExceptionMatcher(String pattern) {
		this.pattern = pattern;
	}

	public String pattern() {
		return this.pattern;
	}
}