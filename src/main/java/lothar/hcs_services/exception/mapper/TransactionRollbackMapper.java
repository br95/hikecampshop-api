package lothar.hcs_services.exception.mapper;

import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJBTransactionRolledbackException;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;

import lothar.hcs_services.exception.matcher.PersistenceExceptionMatcher;

@Provider
public class TransactionRollbackMapper implements ExceptionMapper<EJBTransactionRolledbackException> {

	@Inject
	private Logger logger;
	
	@Override
	public Response toResponse(EJBTransactionRolledbackException e) {
		Throwable rootException = e;
		while (rootException.getCause() != null) {
			rootException = rootException.getCause();
		}
		logger.info(e.getMessage());
		if (rootException.getMessage().matches(PersistenceExceptionMatcher.NOT_NULL_CONSTRAINT_VIOLATION.pattern())) {
			return this.notNullConstraintViolation(rootException);
		} else if (rootException.getMessage().matches(PersistenceExceptionMatcher.UNIQUE_CONSTRAINT_VIOLATION.pattern())) {
			return this.uniqueConstraintViolation(rootException);
		} else if(rootException.getMessage().matches(PersistenceExceptionMatcher.FOREIGN_KEY_CONSTRAINT_VIOLATION.pattern())) {
			return this.foreignKeyConstraint(rootException);
		}
		return Response.serverError().build();
	}

	private Response uniqueConstraintViolation(Throwable rootException) {
		Pattern pattern = Pattern.compile(PersistenceExceptionMatcher.UNIQUE_CONSTRAINT_VIOLATION.pattern());
		Matcher matcher = pattern.matcher(rootException.getMessage());
		Hashtable<String, String> entity = new Hashtable<>();
		while (matcher.find()) {
			entity.put("message", "Unique constraint violation. Transaction with specified field already exists!");
			entity.put("field", matcher.group(1));
			entity.put("value", matcher.group(2));
		}
		return Response.status(400).entity(entity).build();
	}

	private Response notNullConstraintViolation(Throwable rootException) {
		Pattern pattern = Pattern.compile(PersistenceExceptionMatcher.NOT_NULL_CONSTRAINT_VIOLATION.pattern());
		Matcher matcher = pattern.matcher(rootException.getMessage());
		Hashtable<String, String> entity = new Hashtable<>();
		while (matcher.find()) {
			entity.put("message", "Not Null constraint violation. Field cannot be null!");
			entity.put("field", matcher.group(1));
		}
		return Response.status(400).entity(entity).build();
	}
	
	private Response foreignKeyConstraint(Throwable rootException)  {
		Pattern pattern = Pattern.compile(PersistenceExceptionMatcher.FOREIGN_KEY_CONSTRAINT_VIOLATION.pattern());
		Matcher matcher = pattern.matcher(rootException.getMessage());
		Hashtable<String, String> entity = new Hashtable<>();
		while (matcher.find()) {
			entity.put("message", "Foreign key violation! Provjeriti ID-eve!");
			entity.put("field", matcher.group(1));
		}
		return Response.status(400).entity(entity).build();
	}
}