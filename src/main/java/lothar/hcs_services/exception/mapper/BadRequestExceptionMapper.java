package lothar.hcs_services.exception.mapper;

import java.util.Hashtable;

import javax.inject.Inject;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;

import lothar.hcs_services.model.Korisnik;

@Provider
public class BadRequestExceptionMapper implements ExceptionMapper<ClientErrorException> {

	@Inject
	private Logger logger;

	@Inject
	private Korisnik korisnik;

	@Override
	public Response toResponse(ClientErrorException e) {
		logger.info(String.format("Korisnik: %s; Poruka: %s;", korisnik == null ? "JAVNO" : korisnik.getUsername(), e.getMessage()));
		Hashtable<String, String> entity = new Hashtable<>();
		entity.put("message", e.getMessage());
		entity.put("status", new Integer(e.getResponse().getStatus()).toString());
		return Response.status(e.getResponse().getStatus()).entity(entity).type(MediaType.APPLICATION_JSON).build();
	}

}