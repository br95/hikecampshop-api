package lothar.hcs_services.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;



@Entity
@NamedQueries({
	@NamedQuery(name = "ProizvodKomentar", query = "SELECT k from Komentar k JOIN FETCH k.proizvod")
})
public class Komentar implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "KomentarSequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "KomentarSequence", sequenceName = "KomentarSequence", allocationSize = 1)
	private Long id;
   
	private String korisnik;
	@Column(length = 4096)
	private String tekst;
	private Date datumVrijeme;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	private Proizvod proizvod;

// GET / SET
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(String korisnik) {
		this.korisnik = korisnik;
	}

	public String getTekst() {
		return tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}

	public Date getDatumVrijeme() {
		return datumVrijeme;
	}

	public void setDatumVrijeme(Date datumVrijeme) {
		this.datumVrijeme = datumVrijeme;
	}

	public Proizvod getProizvod() {
		return proizvod;
	}

	public void setProizvod(Proizvod proizvod) {
		this.proizvod = proizvod;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Komentar other = (Komentar) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Komentar [id=" + id + ", korisnik=" + korisnik + ", tekst=" + tekst + ", datumVrijeme=" + datumVrijeme
				+ ", proizvod=" + proizvod + "]";
	}
	
	
	
}
