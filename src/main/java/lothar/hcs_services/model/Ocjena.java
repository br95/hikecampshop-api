package lothar.hcs_services.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
public class Ocjena implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "OcjenaSequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "OcjenaSequence", sequenceName = "OcjenaSequence", allocationSize = 1)
	private Long id;
   
	private String korisnik;
	private Integer iznos;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	private Proizvod proizvod;

//	GET/SET
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(String korisnik) {
		this.korisnik = korisnik;
	}

	public Integer getIznos() {
		return iznos;
	}

	public void setIznos(Integer iznos) {
		this.iznos = iznos;
	}

	public Proizvod getProizvod() {
		return proizvod;
	}

	public void setProizvod(Proizvod proizvod) {
		this.proizvod = proizvod;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ocjena other = (Ocjena) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Ocjena [id=" + id + ", korisnik=" + korisnik + ", iznos=" + iznos + ", proizvod=" + proizvod + "]";
	}
	
	
	
}
