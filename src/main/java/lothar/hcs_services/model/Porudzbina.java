package lothar.hcs_services.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
public class Porudzbina implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "PorudzbinaSequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "PorudzbinaSequence", sequenceName = "PorudzbinaSequence", allocationSize = 1)
	private Long id;

	private String korisnik;
	private Float iznos;
	private Date datumVrijeme;
	private Date datumIsporuke;
	
	@ManyToMany
	@JoinTable(name = "porudzbina_proizvod", joinColumns = @JoinColumn(name = "porudzbina_id"), inverseJoinColumns = @JoinColumn(name = "proizvod_id"))
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Set<Proizvod> proizvodi;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(String korisnik) {
		this.korisnik = korisnik;
	}

	public Float getIznos() {
		return iznos;
	}

	public void setIznos(Float iznos) {
		this.iznos = iznos;
	}

	public Date getDatumVrijeme() {
		return datumVrijeme;
	}

	public void setDatumVrijeme(Date datumVrijeme) {
		this.datumVrijeme = datumVrijeme;
	}

	public Date getDatumIsporuke() {
		return datumIsporuke;
	}

	public void setDatumIsporuke(Date datumIsporuke) {
		this.datumIsporuke = datumIsporuke;
	}

	public Set<Proizvod> getProizvodi() {
		return proizvodi;
	}

	public void setProizvodi(Set<Proizvod> proizvodi) {
		this.proizvodi = proizvodi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Porudzbina other = (Porudzbina) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Porudzbina [id=" + id + ", korisnik=" + korisnik + ", iznos=" + iznos + ", datumVrijeme=" + datumVrijeme
				+ ", datumIsporuke=" + datumIsporuke + ", proizvodi=" + proizvodi + "]";
	}

}
