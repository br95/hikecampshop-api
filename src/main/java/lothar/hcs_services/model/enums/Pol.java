package lothar.hcs_services.model.enums;

public enum Pol {
	
	MANS("Mans"),
	WOMEN("Women"),
	CHILD("Children"),
	NEUTRAL("Neutral");
	
	private String name;
	
	private Pol(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
