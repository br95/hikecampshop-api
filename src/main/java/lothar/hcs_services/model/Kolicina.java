package lothar.hcs_services.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Kolicina implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "KolicinaSequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "KolicinaSequence", sequenceName = "KolicinaSequence", allocationSize = 1)
	private Long id;
	
	private Integer brojProizvoda;
	
	@ManyToOne
	@JsonBackReference
	private Proizvod proizvod;
	
	@ManyToOne
	@JsonBackReference
	private Korpa korpa;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Proizvod getProizvod() {
		return proizvod;
	}

	public void setProizvod(Proizvod proizvod) {
		this.proizvod = proizvod;
	}

	public Korpa getKorpa() {
		return korpa;
	}

	public void setKorpa(Korpa korpa) {
		this.korpa = korpa;
	}
	
	

	public Integer getBrojProizvoda() {
		return brojProizvoda;
	}

	public void setBrojProizvoda(Integer brojProizvoda) {
		this.brojProizvoda = brojProizvoda;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kolicina other = (Kolicina) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Kolicina [id=" + id + ", brojProizvoda=" + brojProizvoda + ", proizvod=" + proizvod + ", korpa=" + korpa
				+ "]";
	}
		
}
