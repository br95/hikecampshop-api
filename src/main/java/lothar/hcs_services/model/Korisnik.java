package lothar.hcs_services.model;

import java.util.List;

public interface Korisnik {

	List<String> getRoles();

	String getGroup();

	String getUsername();

	boolean isInRole(String role);
}
