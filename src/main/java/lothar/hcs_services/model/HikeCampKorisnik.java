package lothar.hcs_services.model;

import java.util.List;

public class HikeCampKorisnik implements Korisnik {

	private String username;
	private String group;
	private List<String> roles;

	public HikeCampKorisnik(String username, String group, List<String> roles) {
		this.username = username;
		this.group = group;
		this.roles = roles;
	}

	@Override
	public boolean isInRole(String role) {
		if (roles == null || roles.isEmpty()) {
			return false;
		}
		return roles.stream().filter(userRole-> {
			return userRole.equalsIgnoreCase(role);
		}).findFirst().isPresent();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

}