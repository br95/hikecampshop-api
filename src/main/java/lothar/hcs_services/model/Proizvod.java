package lothar.hcs_services.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lothar.hcs_services.model.enums.Pol;

@Entity
public class Proizvod implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "ProizvodSequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "ProizvodSequence", sequenceName = "ProizvodSequence", allocationSize = 1)
	private Long id;

	private String naziv;
	private Integer trenutnaZaliha;
	private Float cijena;
	private Float porez;
	@Column(length = 4096)
	private String opis;
	private String slikaImgurURL;

	@Enumerated(EnumType.STRING)
	private Pol pol;

	@ManyToOne
	@JsonBackReference
	private Kategorija kategorija;

	@OneToMany(mappedBy = "proizvod", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Komentar> komentari;

	@OneToMany(mappedBy = "proizvod", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Ocjena> ocjene;

	@ManyToMany(mappedBy = "proizvodi")
	@JsonBackReference
	private Set<Korpa> korpe;

	@ManyToMany(mappedBy = "proizvodi")
	@JsonBackReference
	private Set<Porudzbina> porudzbine;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Integer getTrenutnaZaliha() {
		return trenutnaZaliha;
	}

	public void setTrenutnaZaliha(Integer kolicina) {
		this.trenutnaZaliha = kolicina;
	}

	public Float getCijena() {
		return cijena;
	}

	public void setCijena(Float cijena) {
		this.cijena = cijena;
	}

	public Float getPorez() {
		return porez;
	}

	public void setPorez(Float porez) {
		this.porez = porez;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getSlikaImgurURL() {
		return slikaImgurURL;
	}

	public void setSlikaImgurURL(String slikaImgurURL) {
		this.slikaImgurURL = slikaImgurURL;
	}

	public Kategorija getKategorija() {
		return kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}

	public Set<Komentar> getKomentari() {
		return komentari;
	}

	public void setKomentari(Set<Komentar> komentari) {
		this.komentari = komentari;
	}

	public Set<Ocjena> getOcjene() {
		return ocjene;
	}

	public void setOcjene(Set<Ocjena> ocjene) {
		this.ocjene = ocjene;
	}

	public Set<Korpa> getKorpe() {
		return korpe;
	}

	public void setKorpe(Set<Korpa> korpe) {
		this.korpe = korpe;
	}

	public Set<Porudzbina> getPorudzbine() {
		return porudzbine;
	}

	public void setPorudzbine(Set<Porudzbina> porudzbine) {
		this.porudzbine = porudzbine;
	}

	public Pol getPol() {
		return pol;
	}

	public void setPol(Pol pol) {
		this.pol = pol;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proizvod other = (Proizvod) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Proizvod [id=" + id + ", naziv=" + naziv + ", trenutnaZaliha=" + trenutnaZaliha + ", cijena=" + cijena + ", porez="
				+ porez + ", opis=" + opis + ", slikaImgurURL=" + slikaImgurURL + ", pol=" + pol + ", kategorija="
				+ kategorija + ", komentari=" + komentari + ", ocjene=" + ocjene + ", korpe=" + korpe + ", porudzbine="
				+ porudzbine + "]";
	}

}
