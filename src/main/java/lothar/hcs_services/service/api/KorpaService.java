package lothar.hcs_services.service.api;

import lothar.hcs_services.model.Korpa;
import lothar.hcs_services.model.Proizvod;

public interface KorpaService {

	Korpa dajKorisnikovuKorpu();
	Proizvod dodajUkorpu(Long id, Integer kolicina);
	Proizvod ukloniIzKorpe(Long id,Integer kolicina);
	Korpa ocistiKorpu();
}
