package lothar.hcs_services.service.api;

import java.util.Set;

import lothar.hcs_services.model.Kategorija;

public interface KategorijaService {

	Set<Kategorija> sveKategorije(Integer start,Integer end);
	Kategorija dajKategoriju(Long id);
		
}
