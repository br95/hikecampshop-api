package lothar.hcs_services.service.api;

import java.util.Set;

import lothar.hcs_services.model.Porudzbina;
import lothar.hcs_services.model.Proizvod;

public interface PorudzbinaService {

	Set<Porudzbina> vratiSvePorudzbine(Integer start,Integer end);
	Set<Porudzbina> vratiSvePorudzbineKorisnika(Integer start, Integer end);
	Set<Proizvod> vratiProizvodeIzPorudzbine(Long id, Integer start, Integer end);
	Porudzbina vratiPorudzbinu(Long id);
	Porudzbina napraviPorudzbinu();
	
}
