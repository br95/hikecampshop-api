package lothar.hcs_services.service.api;

import java.util.Set;

import lothar.hcs_services.model.Komentar;
import lothar.hcs_services.model.Ocjena;
import lothar.hcs_services.model.Proizvod;

public interface ProizvodiReadService {
	
	Set<Proizvod> vratiProizvode(Integer start, Integer end);
	Proizvod vratiProizvodByID(Long id);
	Set<Proizvod> vratiSveProizvodeIzPorudzbine(Long id, Integer start,Integer end);
	Set<Ocjena> sveOcjeneProizvoda(Long id,Integer start,Integer end);
	Set<Komentar> sviKomentariProizvoda(Long id,Integer start, Integer end);
	Proizvod vratiProizvodByIDwithPorudzbine(Long id);
	Set<Proizvod> vratiProizvodeIzKategorije(Long id, Integer start, Integer end);
	
}
