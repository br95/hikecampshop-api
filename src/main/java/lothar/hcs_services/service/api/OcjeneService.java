package lothar.hcs_services.service.api;

import java.util.Set;

import lothar.hcs_services.model.Ocjena;

public interface OcjeneService {

//	Ocjene read
	Set<Ocjena> sveOcjeneProizvoda(Long proizvodId,Integer start,Integer end);
	Set<Ocjena> sveOcjeneKorisnika(Integer start, Integer end);
	Set<Ocjena> sveOcjeneKorisnikaProizvod(Long proizvodId,Integer start, Integer end);
	Ocjena vratiOcjenuByID(Long ocjenaId);

//	Ocjena write
	Ocjena postaviOcjenu(Long proizvodId, Integer iznos);
	Ocjena izbrisiOcjenu(Long ocjenaId);
	Ocjena izmijeniOcjenu(Long ocjenaId, Integer iznos);
	
}
