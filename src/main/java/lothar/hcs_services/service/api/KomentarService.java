package lothar.hcs_services.service.api;

import java.util.Set;

import lothar.hcs_services.model.Komentar;

public interface KomentarService {

//	Komentar read
	Set<Komentar> sviKomentariProizvoda(Long proizvodId,Integer start,Integer end);
	Set<Komentar> sviKomentariKorisnika(Integer start, Integer end);
	Set<Komentar> sviKomentariKorisnikaProizvod(Long proizvodId,Integer start, Integer end);
	Komentar vratiKomentarById(Long komentarId);

//	Komentar write
	Komentar postaviKomentar(Long proizvodId, String tekst);
	Komentar izbrisiKomentar(Long komentarId);
	Komentar izmijeniKomentar(Long komentarId, String tekst);
}
