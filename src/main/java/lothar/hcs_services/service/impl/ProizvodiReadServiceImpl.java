package lothar.hcs_services.service.impl;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;

import lothar.hcs_services.model.Komentar;
import lothar.hcs_services.model.Ocjena;
import lothar.hcs_services.model.Proizvod;
import lothar.hcs_services.service.api.ProizvodiReadService;
import lothar.hcs_services.util.DaoUtility;

@Stateless
public class ProizvodiReadServiceImpl implements ProizvodiReadService {

	@Inject
	private DaoUtility dao;
	
	@Inject Logger logger;

	@Override
	public Set<Proizvod> vratiProizvode(Integer start, Integer end) {
		logger.logf(Level.DEBUG, "Vracam sve proizvod od %d do %d",start,end);
		Set<Proizvod> rezultat = new LinkedHashSet<>(dao.getPaginatedQuery(Proizvod.class,
				"SELECT DISTINCT p FROM Proizvod p LEFT JOIN FETCH p.komentari LEFT JOIN FETCH p.ocjene LEFT JOIN FETCH p.ocjene ORDER BY p.naziv",
				start, end, null));
		return rezultat;
	}

	@Override
	public Proizvod vratiProizvodByID(Long id) {
		logger.logf(Level.DEBUG, "Vracam proizvod sa id-em %d",id);
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		Proizvod rezultat = dao.getSingleQuery(Proizvod.class,
				"SELECT DISTINCT p from Proizvod p  LEFT JOIN FETCH p.komentari LEFT JOIN FETCH p.ocjene where p.id = :id",
				params);
		return rezultat;
	}
	
	@Override
	public Proizvod vratiProizvodByIDwithPorudzbine(Long id) {
		logger.logf(Level.DEBUG, "Vracam proizvod sa pordzbinama sa id-em %d",id);
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		Proizvod rezultat = dao.getSingleQuery(Proizvod.class,
				"SELECT DISTINCT p from Proizvod p  LEFT JOIN FETCH p.komentari LEFT JOIN FETCH p.ocjene LEFT JOIN FETCH p.porudzbine where p.id = :id",
				params);
		return rezultat;
	}

	@Override
	public Set<Proizvod> vratiSveProizvodeIzPorudzbine(Long id, Integer start, Integer end) {
		logger.logf(Level.DEBUG, "Vracam sve proizvode iz podurzbine sa id-em: %d od %d do %d",id,start,end);
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		Set<Proizvod> rezultat = new LinkedHashSet<>(dao.getPaginatedQuery(Proizvod.class,
				"SELECT DISTINCT p from Proizvod p LEFT JOIN FETCH p.komentari LEFT JOIN FETCH p.ocjene por where por.id = :id  ORDER BY p.naziv",
				start, end, params));
		return rezultat;
	}

	@Override
	public Set<Ocjena> sveOcjeneProizvoda(Long id, Integer start, Integer end) {
		logger.logf(Level.DEBUG, "Vracam sve ocjene od proizvoda sa id-em: %d od %d do %d",id,start,end);
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		Set<Ocjena> rezultat = new LinkedHashSet<>(dao.getPaginatedQuery(Ocjena.class,
				"SELECT DISTINCT o from Ocjena o LEFT JOIN FETCH o.proizvod p where p.id = :id ORDER BY p.naziv", start, end, params));
		return rezultat;
	}

	@Override
	public Set<Komentar> sviKomentariProizvoda(Long id, Integer start, Integer end) {
		logger.logf(Level.DEBUG, "Vracam sve komentare od proizvoda sa id-em: %d od %d do %d",id,start,end);
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		Set<Komentar> rezultat = new LinkedHashSet<>(dao.getPaginatedQuery(Komentar.class,
				"SELECT DISTINCT k from Komentar k LEFT JOIN FETCH k.proizvod p where p.id = :id  ORDER BY p.naziv", start, end, params));
		return rezultat;
	}

	@Override
	public Set<Proizvod> vratiProizvodeIzKategorije(Long id,Integer start, Integer end) {
		logger.logf(Level.DEBUG, "Vracam sve proizvode od kategorije sa id-em: %d od %d do %d",id,start,end);
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		Set<Proizvod> proizvodi = new LinkedHashSet<>(dao.getPaginatedQuery(Proizvod.class, "SELECT p from Proizvod p LEFT JOIN FETCH p.komentari LEFT JOIN FETCH p.ocjene where p.kategorija.id = :id", start, end, params));
		return proizvodi;
	}
}
