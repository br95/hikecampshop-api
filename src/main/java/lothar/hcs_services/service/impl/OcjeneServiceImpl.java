package lothar.hcs_services.service.impl;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;

import lothar.hcs_services.model.Korisnik;
import lothar.hcs_services.model.Ocjena;
import lothar.hcs_services.model.Proizvod;
import lothar.hcs_services.service.api.OcjeneService;
import lothar.hcs_services.service.api.ProizvodiReadService;
import lothar.hcs_services.util.DaoUtility;

@Stateless
public class OcjeneServiceImpl implements OcjeneService {

	@Inject private DaoUtility dao;
	
	@Inject private Logger logger;	
	
	@Inject private Korisnik korisnik;
	
	@Inject private ProizvodiReadService proizvodService;
	
	@Override
	public Set<Ocjena> sveOcjeneProizvoda(Long proizvodId, Integer start, Integer end) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", proizvodId);
		params.put("username", korisnik.getUsername());
		Set<Ocjena> ocjene = new LinkedHashSet<>(dao.getPaginatedQuery(Ocjena.class, "SELECT o from Ocjena o where o.korisnik = :username and o.proizvod.id = :id", start, end, params));
		logger.logf(Level.DEBUG, "Varacam ocjenu korisnika: %s za proizvod sa id-em: %d",korisnik.getUsername(),proizvodId);
		return ocjene;
	}

	@Override
	public Set<Ocjena> sveOcjeneKorisnika(Integer start, Integer end) {
		Map<String, Object> params = new HashMap<>();
		params.put("username", korisnik.getUsername());
		Set<Ocjena> ocjene = new LinkedHashSet<>(dao.getPaginatedQuery(Ocjena.class, "SELECT o from Ocjena o where o.korisnik = :username",start, end, params));
		return ocjene;
	}

	@Override
	public Set<Ocjena> sveOcjeneKorisnikaProizvod(Long proizvodId, Integer start, Integer end) {
		Map<String, Object> params = new HashMap<>();
		params.put("username", korisnik.getUsername());
		params.put("id", proizvodId);
		Set<Ocjena> ocjene = new LinkedHashSet<>(dao.getPaginatedQuery(Ocjena.class, "SELECT o from Ocjena o where o.korisnik = :username and o.proizvod.id = :id", start, end, params));
		return ocjene;
	}	

	@Override
	public Ocjena vratiOcjenuByID(Long ocjenaId) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", ocjenaId);
		Ocjena ocjena = dao.getSingleQuery(Ocjena.class, "SELECT o from Ocjena o where o.id = :id", params);
		return ocjena;
	}

	@Override
	public Ocjena postaviOcjenu(Long proizvodId, Integer iznos) {
		Proizvod proizvod = proizvodService.vratiProizvodByID(proizvodId);
		if(proizvod == null) return null;
		if(proizvod.getOcjene().stream().map(o -> o.getKorisnik()).anyMatch(k -> k.equals(korisnik.getUsername()))) {
			return null;
		}
		Ocjena ocjena = new Ocjena();
		try { 
			dao.savePersist(ocjena);
		} catch(PersistenceException | IllegalArgumentException e) {
			logger.logf(Level.ERROR, "Greska pri persistovanju ocjene korisnika: %s za proizvod sa id-em: %d",korisnik.getUsername(),proizvodId);
			return null;
		}
		ocjena.setKorisnik(korisnik.getUsername());
		ocjena.setIznos(iznos);
		ocjena.setProizvod(proizvod);
		proizvod.getOcjene().add(ocjena);
		try { 
			dao.saveFlush();
		} catch(PersistenceException e) {
			logger.logf(Level.ERROR, "Greska pri flushovanju ocjene korisnika: %s za proizvod sa id-em: %d",korisnik.getUsername(),proizvodId);
			return null;
		}
		return ocjena;
	}

	@Override
	public Ocjena izbrisiOcjenu(Long ocjenaId) {
		Ocjena ocjena = vratiOcjenuByID(ocjenaId);
		if(ocjena == null) return null;
		try {
			dao.saveRemove(ocjena);
			dao.saveFlush();
		} catch(PersistenceException e) {
			logger.logf(Level.DEBUG, "Nesto nije u redu sa brisanjem ocjene sa id-em: %d, poruka: %s", ocjenaId, e.getLocalizedMessage());
			return null;
		}
		return ocjena;
	}

	@Override
	public Ocjena izmijeniOcjenu(Long ocjenaId, Integer iznos) {
		Ocjena ocjena = vratiOcjenuByID(ocjenaId);
		if(ocjena == null) return null;
		ocjena.setIznos(iznos);
		try {
			dao.saveFlush();
		}  catch(PersistenceException e) {
			logger.logf(Level.DEBUG, "Nesto nije u redu sa flushovanjem izmijenjene ocjene sa id-em: %d, poruka: %s", ocjenaId, e.getLocalizedMessage());
			return null;
		}
		return ocjena; 
	}

}
