package lothar.hcs_services.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;

import lothar.hcs_services.model.Kolicina;
import lothar.hcs_services.model.Korisnik;
import lothar.hcs_services.model.Korpa;
import lothar.hcs_services.model.Proizvod;
import lothar.hcs_services.service.api.KorpaService;
import lothar.hcs_services.service.api.ProizvodiReadService;
import lothar.hcs_services.util.DaoUtility;

@Stateless
public class KorpaServiceImpl implements KorpaService {
	
	@Inject private ProizvodiReadService proizvodService;

	@Inject private Korisnik korisnik;
	
	@Inject private DaoUtility dao;
	
	@Inject private Logger logger;

	private Korpa napraviKorpu() {
		logger.log(Level.DEBUG, String.format("Pravim korpu za korisnika %s", korisnik.getUsername()));
		Korpa novaKorpa = new Korpa();
		try {
			dao.savePersist(novaKorpa);
		} catch(PersistenceException e) {
			logger.log(Level.ERROR, String.format("Doslo je do greske prilikom persitovanja korpe, korisnika %s: %s",korisnik.getUsername(),e.getMessage()));
			return null;
		}
		novaKorpa.setKorisnik(korisnik.getUsername());
		novaKorpa.setProizvodi(new LinkedHashSet<>());
		try {
			dao.saveFlush();
		} catch(PersistenceException e) {
			logger.log(Level.ERROR, String.format("Doslo je do greske prilikom flushovanja korpe u bazu, korisnik: %s: %s",korisnik.getUsername(),e.getMessage()));
			return null;
		}
		logger.log(Level.DEBUG, String.format("Korpa uspjesno napravljena %s", korisnik.getUsername()));
		return novaKorpa;
	}
	
	@Override
	public Korpa dajKorisnikovuKorpu() {
		logger.log(Level.DEBUG, String.format("Vracam korpu korisnika: %s", korisnik.getUsername()));
		Map<String,Object> params = new HashMap<>();
		params.put("username", korisnik.getUsername());
		Korpa korisnikovaKorpa = dao.getSingleQuery(Korpa.class, "SELECT DISTINCT k from Korpa k LEFT JOIN FETCH k.proizvodi p LEFT JOIN FETCH p.komentari LEFT JOIN FETCH p.ocjene where k.korisnik = :username", params);
		if(korisnikovaKorpa == null) {
			korisnikovaKorpa = napraviKorpu();
			return korisnikovaKorpa;
		}
		return korisnikovaKorpa;
	}

	@Override
	public Proizvod dodajUkorpu(Long id,Integer kolicina) {
		Map<String,Object> params = new HashMap<>();
		params.put("username", korisnik.getUsername());
		Korpa korisnikovaKorpa = dao.getSingleQuery(Korpa.class, "SELECT DISTINCT k from Korpa k LEFT JOIN FETCH k.proizvodi p LEFT JOIN FETCH p.komentari LEFT JOIN FETCH p.ocjene where k.korisnik = :username", params);
		Proizvod dodati = proizvodService.vratiProizvodByID(id);
		if(dodati == null) return null;
		if(dodati.getTrenutnaZaliha() == 0) return null;
		if(kolicina > dodati.getTrenutnaZaliha()) kolicina = dodati.getTrenutnaZaliha();
		if(dodati.getTrenutnaZaliha() < 1) return null;
		if(korisnikovaKorpa == null) {
			korisnikovaKorpa = napraviKorpu();
			korisnikovaKorpa.getProizvodi().add(dodati);
			dodati.getKorpe().add(korisnikovaKorpa);			
			dodajUkloniBrojProizvodaUkorpi(korisnikovaKorpa, dodati, kolicina,true);
			try {
				dao.saveFlush();
			} catch(PersistenceException e) {
				logger.log(Level.ERROR, String.format("Doslo je do greske prilikom flushovanja korpe u bazu, korisnik: %s: %s",korisnik.getUsername(),e.getMessage()));
				return null;
			}
			return dodati;
		}
		logger.log(Level.DEBUG, String.format("Vracam korpu korisnika: %s, korpa id: %d", korisnik.getUsername(),korisnikovaKorpa.getId()));
		dodajUkloniBrojProizvodaUkorpi(korisnikovaKorpa, dodati, kolicina,true);
		korisnikovaKorpa.getProizvodi().add(dodati);
		dodati.getKorpe().add(korisnikovaKorpa);
		try {
			dao.saveFlush();
		} catch(PersistenceException e) {
			logger.log(Level.ERROR, String.format("Doslo je do greske prilikom flushovanja korpe u bazu, korisnik: %s: %s",korisnik.getUsername(),e.getMessage()));
			return null;
		}
		return dodati;
	}

	@Override
	public Proizvod ukloniIzKorpe(Long id,Integer kolicina) {
		Map<String,Object> params = new HashMap<>();
		params.put("username", korisnik.getUsername());
		Korpa korisnikovaKorpa = dao.getSingleQuery(Korpa.class, "SELECT DISTINCT k from Korpa k LEFT JOIN FETCH k.proizvodi p LEFT JOIN FETCH p.komentari LEFT JOIN FETCH p.ocjene where k.korisnik = :username", params);
		if(korisnikovaKorpa == null) return null;
		Proizvod removed = proizvodService.vratiProizvodByID(id);
		if(removed == null) return null;
		if(kolicina != null) {
			dodajUkloniBrojProizvodaUkorpi(korisnikovaKorpa,removed,kolicina,false);
		} 
		if(korisnikovaKorpa.getProizvodi().remove(removed)) {
			ukloniKolicinuProizvoda(korisnikovaKorpa, removed);
			removed.getKorpe().remove(korisnikovaKorpa);				
		}
		try {
			dao.saveFlush();
			return removed;
		} catch(PersistenceException e) {
			logger.log(Level.ERROR, String.format("Doslo je do greske prilikom flushovanja korpe u bazu, korisnik: %s: %s",korisnik.getUsername(),e.getMessage()));
			return null;
		}
	}

	@Override
	public Korpa ocistiKorpu() {
		Map<String,Object> params = new HashMap<>();
		params.put("username", korisnik.getUsername());
		Korpa korisnikovaKorpa = dao.getSingleQuery(Korpa.class, "SELECT DISTINCT k from Korpa k LEFT JOIN FETCH k.proizvodi p LEFT JOIN FETCH p.komentari LEFT JOIN FETCH p.ocjene where k.korisnik = :username", params);
		if(korisnikovaKorpa == null) {
			korisnikovaKorpa = napraviKorpu();
			return korisnikovaKorpa;
		}
		ukloniKolicinuProizvoda(korisnikovaKorpa, null);
		for(Proizvod p : korisnikovaKorpa.getProizvodi()) 
			p.getKorpe().remove(korisnikovaKorpa);
		korisnikovaKorpa.setProizvodi(new LinkedHashSet<>());
		try {
			dao.saveFlush();
		} catch(PersistenceException e) {
			logger.log(Level.ERROR, String.format("Doslo je do greske prilikom flushovanja korpe u bazu, korisnik: %s: %s",korisnik.getUsername(),e.getMessage()));
			return null;
		}
		return korisnikovaKorpa;
	}
	
// -------------------------------------------------------------------------------------------
	private void dodajUkloniBrojProizvodaUkorpi(Korpa korpa, Proizvod proizvod, Integer kolicina,boolean dodaj) {
		Map<String,Object> params = new HashMap<>();
		params.put("idKorpa", korpa.getId());
		params.put("idProizvod", proizvod.getId());
		Kolicina kolProizvodaUkorpi = dao.getSingleQuery(Kolicina.class, "SELECT k from Kolicina k where k.korpa.id = :idKorpa and k.proizvod.id = :idProizvod", params);
		if(kolProizvodaUkorpi != null) {
			if(dodaj) {
				kolProizvodaUkorpi.setBrojProizvoda(kolProizvodaUkorpi.getBrojProizvoda() + kolicina);
			} else if(kolProizvodaUkorpi.getBrojProizvoda() <= kolicina) {
				ukloniKolicinuProizvoda(korpa, proizvod);
			}
			else {
				kolProizvodaUkorpi.setBrojProizvoda(kolProizvodaUkorpi.getBrojProizvoda() - kolicina);
			}
		} else {
			kolProizvodaUkorpi = new Kolicina();
			kolProizvodaUkorpi.setKorpa(korpa);
			kolProizvodaUkorpi.setProizvod(proizvod);
			kolProizvodaUkorpi.setBrojProizvoda(kolicina);	
		}
		try {
			dao.savePersist(kolProizvodaUkorpi);
		} catch(PersistenceException e) {
			logger.log(Level.ERROR, String.format("Doslo je do greske prilikom flushovanja korpe u bazu, korisnik: %s: %s",korisnik.getUsername(),e.getMessage()));
		}
	}
	
	private void ukloniKolicinuProizvoda(Korpa korpa, Proizvod proizvod) {
		Map<String,Object> params = new HashMap<>();
		params.put("idKorpa", korpa.getId());
		if(proizvod == null) {
			List<Kolicina> kolicineKorpe =  new ArrayList<>(dao.getAllQuery(Kolicina.class, "SELECT k from Kolicina k where k.korpa.id = :idKorpa", params));
			for(Kolicina kolicina : kolicineKorpe) {
				try {
					dao.saveRemove(kolicina);
				} catch(PersistenceException e) {
					logger.log(Level.ERROR, String.format("Doslo je do greske prilikom remova kolicine proizvoda iz baze, korisnik: %s: %s",korisnik.getUsername(),e.getMessage()));
				}
			}
			return;
		} 
		params.put("idProizvod", proizvod.getId());
		Kolicina kolicinaProizvoda = dao.getSingleQuery(Kolicina.class, "SELECT DISTINCT k from Kolicina k where k.proizvod.id = :idProizvod and k.korpa.id = :idKorpa", params);
		try {
			dao.saveRemove(kolicinaProizvoda);
		} catch(PersistenceException e) {
			logger.log(Level.ERROR, String.format("Doslo je do greske prilikom remova kolicine proizvoda iz baze, korisnik: %s: %s",korisnik.getUsername(),e.getMessage()));
		}
	}

}
