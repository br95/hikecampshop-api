package lothar.hcs_services.service.impl;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import lothar.hcs_services.model.Kategorija;
import lothar.hcs_services.service.api.KategorijaService;
import lothar.hcs_services.util.DaoUtility;

@Stateless
public class KategorijaServiceImpl implements KategorijaService{

	@Inject private DaoUtility dao;
		
	@Inject private Logger logger;
	
	@Override
	public Set<Kategorija> sveKategorije(Integer start, Integer end) {
		logger.debug("Vracam sve kategorije");
		return new LinkedHashSet<>(dao.getPaginatedQuery(Kategorija.class, "SELECT k from Kategorija k", start, end, null));
	}

	@Override
	public Kategorija dajKategoriju(Long id) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);
		return dao.getSingleQuery(Kategorija.class, "SELECT k from Kategorija k where k.id = :id", params);
	}

}
