package lothar.hcs_services.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;

import lothar.hcs_services.model.Kolicina;
import lothar.hcs_services.model.Korisnik;
import lothar.hcs_services.model.Korpa;
import lothar.hcs_services.model.Porudzbina;
import lothar.hcs_services.model.Proizvod;
import lothar.hcs_services.service.api.KorpaService;
import lothar.hcs_services.service.api.PorudzbinaService;
import lothar.hcs_services.util.DaoUtility;

@Stateless
public class PorudzbinaServiceImpl implements PorudzbinaService {

	@Inject private DaoUtility dao;
	
	@Inject private Logger logger;
		
	@Inject private KorpaService korpaService;
	
	@Inject private Korisnik korisnik;

	
	@Override
	public Set<Porudzbina> vratiSvePorudzbine(Integer start, Integer end) {
		logger.log(Level.DEBUG, "Vracam sve porudzbine");
		Set<Porudzbina> rezultat = new LinkedHashSet<>(dao.getPaginatedQuery(Porudzbina.class, "SELECT DISTINCT p from Porudzbina p LEFT JOIN FETCH p.proizvodi pro LEFT JOIN FETCH pro.komentari LEFT JOIN FETCH pro.ocjene",start,end, null));
		return rezultat;
	}

	@Override
	public Set<Porudzbina> vratiSvePorudzbineKorisnika(Integer start, Integer end) {
		logger.log(Level.DEBUG, String.format("Vracam sve porudzbine korisnika: %s", korisnik.getUsername()));
		Map<String,Object> params = new HashMap<>();
		params.put("username", korisnik.getUsername());
		if(korisnik == null) return null;
		Set<Porudzbina> rezultat = new LinkedHashSet<>(dao.getAllQuery(Porudzbina.class, "SELECT DISTINCT p from Porudzbina p LEFT JOIN FETCH p.proizvodi pro LEFT JOIN FETCH pro.komentari LEFT JOIN FETCH pro.ocjene where p.korisnik = :username", params));
		return rezultat;
	}

	@Override
	public Porudzbina vratiPorudzbinu(Long id) {
		logger.log(Level.DEBUG, String.format("Vracam porudzbinu sa id-em: %d", id));
		Map<String,Object> params = new HashMap<>();
		params.put("id", id);
		Porudzbina rezultat = dao.getSingleQuery(Porudzbina.class, "SELECT DISTINCT p from Porudzbina p LEFT JOIN FETCH p.proizvodi pro LEFT JOIN FETCH pro.komentari LEFT JOIN FETCH pro.ocjene where p.id = :id ", params);
		return rezultat;
	}
	
	@Override
	public Set<Proizvod> vratiProizvodeIzPorudzbine(Long id, Integer start, Integer end) {
		logger.log(Level.DEBUG, String.format("Vracam proizvode iz porudzbine sa id-em: %d, korisnik: %s", id,korisnik.getUsername()));
		Map<String,Object> params = new HashMap<>();
		params.put("id", id);
		Set<Proizvod> rezultat = new LinkedHashSet<>(dao.getPaginatedQuery(Proizvod.class, "SELECT DISTINCT p from Proizvod p LEFT JOIN FETCH p.komentari LEFT JOIN FETCH p.ocjene LEFT JOIN FETCH p.porudzbine por where por.id = :id", start, end, params));
		return rezultat;
	}

	@Override
	public Porudzbina napraviPorudzbinu() {
		Korpa korisnikovaKorpa = korpaService.dajKorisnikovuKorpu();
		if(korisnikovaKorpa == null) {
			return null;
		}
		Porudzbina porudzbina = new Porudzbina();
		try {
			dao.savePersist(porudzbina);
		} catch(PersistenceException e) {
			logger.log(Level.ERROR, String.format("Greska prilikom kreiranja porudzbine, korisnik: %s, greska: %s", korisnik.getUsername(),e.getLocalizedMessage()));
		}
		porudzbina.setDatumVrijeme(new Date());
		porudzbina.setKorisnik(korisnik.getUsername());
		porudzbina.setDatumIsporuke(izracunatiDatumIsporuke(porudzbina));
		porudzbina.setIznos(izracunajIznos(korisnikovaKorpa));
		porudzbina.setProizvodi(new LinkedHashSet<>(korisnikovaKorpa.getProizvodi()));
		ocisitiKolicineKorpe(korisnikovaKorpa);
		korpaService.ocistiKorpu();
		try {
			dao.saveFlush();
		} catch(PersistenceException e) {
			logger.logf(Level.ERROR, "Ne moze se flashovati porudzbina sa id-em: %d",porudzbina.getId());
		}
		return porudzbina;

	}
	
	private Float izracunajIznos(Korpa korpa) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", korpa.getId());
		List<Kolicina> kolicineKorpe = dao.getAllQuery(Kolicina.class,"SELECT k from Kolicina k where k.korpa.id = :id", params); 
		Float iznos = kolicineKorpe
					.stream()
					.map(kolicina -> kolicina.getBrojProizvoda() * kolicina.getProizvod().getCijena())
					.reduce(0f, (s,n) -> s + n);
		return iznos;
	}
	
	private Date izracunatiDatumIsporuke(Porudzbina por) {
		// TODO: smisliti kako da odradim da odredjuje koliko prosjecno treba svakom proizvodu da stigne 
		return Date.from(LocalDate.now().plusDays(7).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}
	
	private void ocisitiKolicineKorpe(Korpa korpa) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", korpa.getId());
		List<Kolicina> kolProizvodaKorpa = dao.getAllQuery(Kolicina.class, "SELECT k from Kolicina k where k.korpa.id = :id", params);
		if(kolProizvodaKorpa == null) return;
		for(Proizvod proizvod : korpa.getProizvodi()) {
			for(Kolicina k : kolProizvodaKorpa) {
				if(k.getProizvod().equals(proizvod)) {
					proizvod.setTrenutnaZaliha(proizvod.getTrenutnaZaliha() - k.getBrojProizvoda());
					try {
						dao.saveMerge(proizvod);
						dao.saveRemove(k);
					} catch(PersistenceException e) {
						logger.logf(Level.ERROR, "Ne moze se ukloniti kolicina sa id-em: %d",k.getId());
					}
				}
			}
		}
	}

}
