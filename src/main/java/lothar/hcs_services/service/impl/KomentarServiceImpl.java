package lothar.hcs_services.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;

import lothar.hcs_services.model.Komentar;
import lothar.hcs_services.model.Korisnik;
import lothar.hcs_services.model.Proizvod;
import lothar.hcs_services.service.api.KomentarService;
import lothar.hcs_services.service.api.ProizvodiReadService;
import lothar.hcs_services.util.DaoUtility;

@Stateless
public class KomentarServiceImpl implements KomentarService {

	@Inject private Korisnik korisnik;
	
	@Inject private ProizvodiReadService proizvodService;
	
	@Inject private DaoUtility dao;
	
	@Inject private Logger logger;
	
	@Override
	public Set<Komentar> sviKomentariProizvoda(Long proizvodId, Integer start, Integer end) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", proizvodId);
		params.put("username", korisnik.getUsername());
		Set<Komentar> komentari = new LinkedHashSet<>(dao.getPaginatedQuery(Komentar.class, "SELECT k from Komentar k where k.korisnik = :username and k.proizvod.id = :id", start, end, params));
		logger.logf(Level.DEBUG, "Varacam komentar korisnika: %s za proizvod sa id-em: %d",korisnik.getUsername(),proizvodId);
		return komentari;
	}

	@Override
	public Set<Komentar> sviKomentariKorisnika(Integer start,Integer end) {
		// TODO: Fix not changing user after refreshing token with other credentials
		Map<String, Object> params = new HashMap<>();
		params.put("username", korisnik.getUsername());
		Set<Komentar> komentari = new LinkedHashSet<>(dao.getPaginatedQuery(Komentar.class, "SELECT k from Komentar k where k.korisnik = :username", start, end, params));
		return komentari;
	}

	@Override
	public Set<Komentar> sviKomentariKorisnikaProizvod(Long proizvodId, Integer start, Integer end) {
		Map<String, Object> params = new HashMap<>();
		params.put("username", korisnik.getUsername());
		params.put("id", proizvodId);
		Set<Komentar> komentari = new LinkedHashSet<>(dao.getPaginatedQuery(Komentar.class, "SELECT k from Komentar k where k.korisnik = :username and k.proizvod.id = :id", start, end, params));
		return komentari;
	}

	@Override
	public Komentar vratiKomentarById(Long komentarId) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", komentarId);
		Komentar komentar = dao.getSingleQuery(Komentar.class, "SELECT k from Komentar k where k.id = :id", params);
		return komentar;
	}

	@Override
	public Komentar postaviKomentar(Long proizvodId, String tekst) {
		Proizvod proizvod = proizvodService.vratiProizvodByID(proizvodId);
		if(proizvod == null) {
			logger.logf(Level.DEBUG,"Proizvod sa id-em: %d ne postoji",proizvodId);
			return null;
		}
		Komentar noviKomentar = new Komentar();
		try {
			dao.savePersist(noviKomentar);
		} catch(PersistenceException | IllegalArgumentException e) {
			logger.logf(Level.DEBUG,"Nesupjesno persistovanje komentara, poruka: %s",e.getLocalizedMessage());
			return null;
		}
		noviKomentar.setDatumVrijeme(new Date());
		noviKomentar.setKorisnik(korisnik.getUsername());
		noviKomentar.setProizvod(proizvod);
		noviKomentar.setTekst(tekst);
		proizvod.getKomentari().add(noviKomentar);
		try {
			dao.saveFlush();
		} catch(PersistenceException e) {
			logger.logf(Level.DEBUG,"Nesupjesno flushovanje podataka, poruka: %s",e.getLocalizedMessage());
			return null;
		}
		return noviKomentar;
	}

	@Override
	public Komentar izbrisiKomentar(Long komentarId) {
		Komentar komentar = vratiKomentarById(komentarId);
		if(komentar == null) return null;
		try {
			dao.saveRemove(komentar);
			dao.saveFlush();
		} catch(PersistenceException e) {
			logger.logf(Level.DEBUG, "Nesto nije u redu sa brisanjem komentar sa id-em: %d, poruka: %s", komentarId, e.getLocalizedMessage());
			return null;
		}
		return komentar;
	}

	@Override
	public Komentar izmijeniKomentar(Long komentarId, String tekst) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", komentarId);
		Komentar komentar = dao.getSingleQuery(Komentar.class, "SELECT k from Komentar k where k.id = :id", params);
		if(komentar == null) return null;
		komentar.setTekst(tekst);
		komentar.setDatumVrijeme(new Date());
		try {
			dao.saveFlush();
		} catch(PersistenceException e) {
			logger.logf(Level.DEBUG, "Nesto nije u redu sa updejtovanjem komentara sa id-em: %d, poruka: %s", komentarId, e.getLocalizedMessage());
			return null;
		}
		return komentar;
	}

}
