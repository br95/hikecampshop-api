 package lothar.hcs_services.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import me.coreit.security.api.keycloak.ClientId;
import me.coreit.security.api.keycloak.ClientSecret;
import me.coreit.security.api.keycloak.ClientUUID;
import me.coreit.security.api.keycloak.KeycloakURL;
import me.coreit.security.api.keycloak.RealmName;

@ApplicationScoped
public class KeycloakProducer {

	@Produces
	@KeycloakURL
	private final String KEYCLOAK_URL = System.getProperty("keycloak.hcs-services.url");

	@Produces
	@RealmName
	private final String REALM_NAME = System.getProperty("keycloak.hcs-services.realm");

	@Produces
	@ClientId
	private final String CLIENT_ID = System.getProperty("keycloak.hcs-services.client.id");

	@Produces
	@ClientUUID
	private final String CLIENT_UUID = System.getProperty("keycloak.hcs-services.client.uuid");

	@Produces
	@ClientSecret
	private final String CLIENT_SECRET = System.getProperty("keycloak.hcs-services.client.secret");

}
