package lothar.hcs_services.util;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

@Stateless
public class DaoUtility implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager em;

	@Inject
	Logger logger;

	public <T> T getById(Class<T> clazz, Long id) {
		return em.find(clazz, id);
	}
	
	public <T> void savePersist(T t) throws IllegalArgumentException, PersistenceException {
		em.persist(t);
	}
	
	public <T> void saveRemove(T t) throws IllegalArgumentException, PersistenceException {
		em.remove(t);
	}
	
	public void saveFlush() throws PersistenceException {
		em.flush();
	}

	public <T> T saveMerge(T t) throws IllegalArgumentException {
		return em.merge(t);
	}

	private <T> TypedQuery<T> buildNamedQuery(Class<T> clazz, String namedQuery, Map<String, Object> params) {
		return buildNamedQuery(clazz, namedQuery, params, null, null);
	}

	private <T> TypedQuery<T> buildNamedQuery(Class<T> clazz, String namedQuery, Map<String, Object> params, Integer offset, Integer limit) {
		TypedQuery<T> query = em.createNamedQuery(namedQuery, clazz);
		if (params != null) {
			for (Entry<String, Object> t : params.entrySet()) {
				query.setParameter(t.getKey(), t.getValue());
			}
		}

		if (offset != null) {
			query.setFirstResult(offset);
		}

		if (limit != null) {
			query.setMaxResults(limit);
		}

		return query;
	}

	public <T> Query buildQuery(Class<T> clazz, String querystring) throws IllegalArgumentException {
		return em.createQuery(querystring, clazz);
	}

	private <T> TypedQuery<T> buildQuery(Class<T> clazz, String queryString, Map<String, Object> params) throws IllegalArgumentException {
		return buildQuery(clazz, queryString, params, null, null);
	}

	private <T> TypedQuery<T> buildQuery(Class<T> clazz, String queryString, Map<String, Object> params, Integer offset, Integer limit)
			throws IllegalArgumentException {
		TypedQuery<T> query = null;

		query = em.createQuery(queryString, clazz);

		if (params != null) {
			for (Entry<String, Object> t : params.entrySet()) {
				query.setParameter(t.getKey(), t.getValue());
			}
		}

		if (offset != null) {
			query.setFirstResult(offset);
		}

		if (limit != null) {
			query.setMaxResults(limit);
		}

		return query;
	}

	public <T> List<T> getAllNamedQuery(Class<T> clazz, String namedQuery, Map<String, Object> params) {
		TypedQuery<T> query = buildNamedQuery(clazz, namedQuery, params);
		return (List<T>) query.getResultList();
	}

	public <T> List<T> getAllNamedQuery(Class<T> clazz, String namedQuery, Map<String, Object> params, Integer offset, Integer limit) {
		TypedQuery<T> query = buildNamedQuery(clazz, namedQuery, params, offset, limit);
		return (List<T>) query.getResultList();
	}

	public <T> T getSingleNamedQuery(Class<T> clazz, String namedQuery, Map<String, Object> params) {
		TypedQuery<T> query = buildNamedQuery(clazz, namedQuery, params);
		query = buildNamedQuery(clazz, namedQuery, params);
		return query.getResultList().stream().findFirst().orElse(null);
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getNativeQuery(String query, Map<String, Object> params) {
		Query query1 = em.createNativeQuery(query);
		if (params != null) {
			for (Entry<String, Object> t : params.entrySet()) {
				query1.setParameter(t.getKey(), t.getValue());
			}
		}
		return (List<Object[]>) query1.getResultList();
	}

	public Object getNativeQueryObject(String query, Map<Integer, Object> params) {
		Query query1 = em.createNativeQuery(query);
		if (params != null) {
			for (Entry<Integer, Object> t : params.entrySet()) {
				query1.setParameter(t.getKey(), t.getValue());
			}
		}
		return (Object) query1.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getNativeQueryInt(String query, Map<Integer, Object> params) {
		Query query1 = em.createNativeQuery(query);
		if (params != null) {
			for (Entry<Integer, Object> t : params.entrySet()) {
				query1.setParameter(t.getKey(), t.getValue());
			}
		}
		query1.getResultList();
		return (List<Object[]>) query1.getResultList();
	}

	public int getCountNamedQuery(String query, Map<String, Object> params) {
		Query query1 = em.createNamedQuery(query);
		if (params != null) {
			for (Entry<String, Object> t : params.entrySet()) {
				query1.setParameter(t.getKey(), t.getValue());
			}
		}
		try {
			return ((Number) query1.getSingleResult()).intValue();
		} catch (NoResultException e) {
			return 0;
		}
	}

	public int getCountQuery(String query, Map<String, Object> params) {
		Query query1 = em.createQuery(query);
		if (params != null) {
			for (Entry<String, Object> t : params.entrySet()) {
				query1.setParameter(t.getKey(), t.getValue());
			}
		}
		try {
			return ((Number) query1.getSingleResult()).intValue();
		} catch (NoResultException e) {
			return 0;
		}
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getPaginatedNamedQuery(Class<T> clazz, String namedQuery, int startRow, int maxRecords, Map<String, Object> params) {
		TypedQuery<T> typedQuery = null;

		try {
			typedQuery = buildNamedQuery(clazz, namedQuery, params);
		} catch (IllegalArgumentException e) {
			logger.error("Greška prilikom izvršenja query-ja za getPaginatedNamedQuery! ");
			e.printStackTrace();
			throw new PersistenceException("Grepška u getPaginatedNamedQuery-ju");
		}

		typedQuery.setMaxResults(maxRecords);
		typedQuery.setFirstResult(startRow);
		return (List<T>) typedQuery.getResultList();
	}

	public <T> List<T> getPaginatedQuery(Class<T> clazz, String query, int startRow, int maxRecords, Map<String, Object> params) {
		TypedQuery<T> typedQuery = null;

		try {
			typedQuery = buildQuery(clazz, query, params);
		} catch (IllegalArgumentException e) {
			logger.error("Greška prilikom izvršenja query-ja za getPaginatedQuery! ");
			e.printStackTrace();
			throw new PersistenceException("Grepška u getPaginatedQuery-ju");
		}

		typedQuery.setFirstResult(startRow);
		typedQuery.setMaxResults(maxRecords);

		return (List<T>) typedQuery.getResultList();
	}

	public <T> T getSingleQuery(Class<T> clazz, String queryString, Map<String, Object> params) {
		TypedQuery<T> typedQuery = null;

		try {
			typedQuery = buildQuery(clazz, queryString, params);
		} catch (IllegalArgumentException e) {
			logger.error("Greška prilikom izvršenja query-ja za getSingleQuery! ");
			e.printStackTrace();
			throw new PersistenceException("Grepška u getSingleQuery-ju");
		}

		List<T> list = typedQuery.getResultList();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public <T> List<T> getAllQuery(Class<T> clazz, String queryString, Map<String, Object> params) {
		TypedQuery<T> typedQuery = null;
		try {
			typedQuery = buildQuery(clazz, queryString, params);
		} catch (IllegalArgumentException e) {
			logger.error("Greška prilikom izvršenja query-ja za getAllQuery! ");
			e.printStackTrace();
			throw new PersistenceException("Grepška u getAllQuery-ju");
		}
		return (List<T>) typedQuery.getResultList();
	}
}