package lothar.hcs_services.rest.javno;

import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lothar.hcs_services.model.Kategorija;
import lothar.hcs_services.service.api.KategorijaService;

@Path("javno/kategorije")
public class KategorijaResource {

	@Inject private KategorijaService kategorijaService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response dajKategorije(@QueryParam("start") @DefaultValue("0") Integer start,@QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Kategorija> rezultat = kategorijaService.sveKategorije(start, end);
		if(rezultat == null) {
			throw new BadRequestException("Kategorije ne postoje ili je doslo do neke greske u servisu");
		}
		return Response.ok(rezultat).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON) 
	public Response dajKategoriju(@PathParam("id") Long id) {
		Kategorija rezultat = kategorijaService.dajKategoriju(id);
		if(rezultat == null) {
			throw new BadRequestException(String.format("Kategorija sa id-em: %d", id));
		}
		return Response.ok(rezultat).build();
	}
	
	
}
