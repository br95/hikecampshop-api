package lothar.hcs_services.rest.javno;

import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;

import lothar.hcs_services.model.Komentar;
import lothar.hcs_services.model.Ocjena;
import lothar.hcs_services.model.Proizvod;
import lothar.hcs_services.service.api.ProizvodiReadService;

@Path("javno/proizvodi")
public class ProizvodiResource {

	@Inject
	private ProizvodiReadService proizvodiReadService;

	@Inject
	private Logger log;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response vratiSveProizvode(@QueryParam("start") @DefaultValue("1") Integer start,
			@QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Proizvod> sviProizvodi = proizvodiReadService.vratiProizvode(start, end);
		log.infov("Svi proizvodi koji su ucitani");
		;
		if (sviProizvodi == null || sviProizvodi.isEmpty()) {
			throw new BadRequestException("Nema proizvoda u bazi");
		}
		return Response.ok(sviProizvodi).build();
	}
	
	@GET
	@Path("/kategorija/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vratiProizvodeIzKategorije(@PathParam("id") Long id,@QueryParam("start") @DefaultValue("1") Integer start,
			@QueryParam("end") @DefaultValue("50") Integer end) { 
		Set<Proizvod> rezultat = proizvodiReadService.vratiProizvodeIzKategorije(id, start, end);
		if(rezultat == null || rezultat.isEmpty()) {
			throw new BadRequestException("Kategorija ne sadrzi proizvode ili parametri nisu u redu");
		}
		return Response.ok(rezultat).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vratiProizvodByID(@PathParam("id") Long id) {
		Proizvod rezultat = proizvodiReadService.vratiProizvodByID(id);
		if (rezultat == null) {
			throw new BadRequestException(String.format("Proizvod sa id-em %d ne postoji", id));
		}
		return Response.ok(rezultat).build();
	}

	@GET
	@Path("/{id}/porudzbine")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vratiProizvodeIzPorudzbine(@PathParam("id") Long id,@QueryParam("start") @DefaultValue("0") Integer start,
			@QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Proizvod> rezultat = proizvodiReadService.vratiSveProizvodeIzPorudzbine(id,start,end);
		if (rezultat == null || rezultat.isEmpty()) {
			throw new BadRequestException(String.format("Porudzbina sa id-em: %d nema proizvoda ili ne postoji", id));
		}
		return Response.ok(rezultat).build();
	}

	@GET
	@Path("/{id}/komentari")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vratiSveKomentare(@PathParam("id") Long id, @QueryParam("start") @DefaultValue("0") Integer start,
			@QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Komentar> rezultat = proizvodiReadService.sviKomentariProizvoda(id, start, end);
		if (rezultat == null || rezultat.isEmpty()) {
			throw new BadRequestException(
					String.format("Komentari za proizvod sa id-em: %d ne postoje ili je neka druga greska", id));
		}
		return Response.ok(rezultat).build();
	}

	@GET
	@Path("/{id}/ocjene")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vratiSveOcjene(@PathParam("id") Long id,@QueryParam("start") @DefaultValue("0") Integer start,
			@QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Ocjena> rezultat = proizvodiReadService.sveOcjeneProizvoda(id,start,end);
		if (rezultat == null || rezultat.isEmpty()) {
			throw new BadRequestException(
					String.format("Ocjene za proizvod sa id-em: %d ne postoje ili je neka druga greska", id));
		}
		return Response.ok(rezultat).build();
	}
}
