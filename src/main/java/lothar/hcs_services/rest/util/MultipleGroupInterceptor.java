package lothar.hcs_services.rest.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import org.keycloak.KeycloakPrincipal;

import lothar.hcs_services.model.HikeCampKorisnik;
import lothar.hcs_services.model.Korisnik;
import me.coreit.security.api.keycloak.ClientId;

@Provider
public class MultipleGroupInterceptor implements ContainerRequestFilter {

	@Inject
	private HttpServletRequest request;

	@Context
	private UriInfo uriInfo;

	@Produces
	private Korisnik korisnik;

	@Inject
	@ClientId
	private String clientId;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		if (uriInfo.getPath().startsWith("/javno/")) {
			return;
		}

		KeycloakPrincipal principal = ((KeycloakPrincipal) request.getUserPrincipal());
		if (principal == null) {
			throw new ForbiddenException("Korisnikov principal je null!");
		}
		String username = principal.getName();
		Map<String, Object> mapa = principal.getKeycloakSecurityContext().getToken().getOtherClaims();
		List<String> roles = new ArrayList<>(principal.getKeycloakSecurityContext().getToken().getResourceAccess(clientId).getRoles());
		List<String> groups = (List<String>) mapa.get("groups");
		if (groups.size() != 1) {
			throw new BadRequestException("Korisnik smije pripadati samo jednoj grupi!");
		}
		String group = groups.get(0);
		korisnik = new HikeCampKorisnik(username, group, roles);
	}

}