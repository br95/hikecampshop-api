package lothar.hcs_services.rest.users;

import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lothar.hcs_services.model.Ocjena;
import lothar.hcs_services.service.api.OcjeneService;

@Path("users/ocjene")
public class OcjeneResource {
	
	@Inject private OcjeneService ocjeneService;

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response sveOcjeneKorisnika(@QueryParam("start") @DefaultValue("0") Integer start, @QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Ocjena> rezultat = ocjeneService.sveOcjeneKorisnika(start, end);
		if(rezultat == null || rezultat.isEmpty()) {
			throw new BadRequestException("Korisnik nije nista ocjenjivao ili ima neka greska");
		}
		return Response.ok(rezultat).build();
	}
	
	@GET
	@Path("/proizvod/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response sveOcjeneProizvoda(@PathParam("id") Long proizvodId,
			@QueryParam("start") @DefaultValue("0") Integer start, @QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Ocjena> rezultat = ocjeneService.sveOcjeneProizvoda(proizvodId, start, end);
		if(rezultat == null || rezultat.isEmpty()) {
			throw new BadRequestException("Ocjene za ovaj proizvod ne postoje ili ste unijeli pogresne parametre");
		}
		return Response.ok(rezultat).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vratiKomentarByID(@PathParam("id") Long ocjenaId) {
		Ocjena rezultat = ocjeneService.vratiOcjenuByID(ocjenaId);
		if(rezultat == null) {
			throw new BadRequestException("Ocjena sa datim id-em ne postoji ili niste unijeli dobre paramtere!");
		}
		return Response.ok(rezultat).build();
	}
	
	@GET
	@Path("/proizvod/{id}/korisnik")
	@Produces(MediaType.APPLICATION_JSON)
	public Response sviKomentariKorisnikaProizvod(@PathParam("id") Long proizvodId,
			@QueryParam("start") @DefaultValue("0") Integer start, @QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Ocjena> rezultat = ocjeneService.sveOcjeneKorisnikaProizvod(proizvodId, start, end);
		if(rezultat == null || rezultat.isEmpty()) {
			throw new BadRequestException("Ocjene za ovaj proizvod ili ovog korisnika ne postoje ili ste unijeli pogresne parametre");
		}
		return Response.ok(rezultat).build();
	}
	
// ----------------------------------------------------
	
	@POST
	@Path("/proizvod/{id}/{iznos}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response postaviOcjenu(@PathParam("id") Long proizvodId,@PathParam("iznos") Integer iznos) { 
		if(iznos == null) {
			throw new BadRequestException("Iznos ocjene nije poslat!");
		}
		Ocjena rezultat = ocjeneService.postaviOcjenu(proizvodId,iznos);
		if(rezultat == null) {
			throw new BadRequestException("Ocjena nije uspjesno postavljena,  vec ste je postavili za ovaj proizvod ili je doslo je do neke greske");
		}
		return Response.ok(rezultat).build();
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response izbrisiOcjenu(@PathParam("id") Long ocjenaId) {
		Ocjena rezultat = ocjeneService.izbrisiOcjenu(ocjenaId);
		if(rezultat == null) {
			throw new BadRequestException("Ocjena koji pokusavate da izbrisete ne postoji ili nije dobar id koji ste unijeli");			
		}
		return Response.ok(rezultat).build();
	}
	
	@PUT
	@Path("/{id}/{iznos}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response izmijeniKomentar(@PathParam("id") Long ocjenaId,@PathParam("iznos") Integer iznos) {
		Ocjena rezultat = ocjeneService.izmijeniOcjenu(ocjenaId, iznos);
		if(rezultat == null) {
			throw new BadRequestException("Ocjena koji pokusavate da izmijenite ne postoji ili nije vas pa ne mozete da ga mijenjate");		
		}
		return Response.ok(rezultat).build();
	}
}
