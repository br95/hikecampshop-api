package lothar.hcs_services.rest.users;

import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lothar.hcs_services.model.Porudzbina;
import lothar.hcs_services.model.Proizvod;
import lothar.hcs_services.service.api.PorudzbinaService;

@Path("users/porudzbine")
public class PorudzbineResource {

	@Inject
	private PorudzbinaService porudzbinaService;

	//TODO: ovaj resurs prebaciti u admina zato sto ne mogu svi vidjti sve porudzbine
	
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response vratiSvePorudzbine(@QueryParam("start") @DefaultValue("0") Integer start,
//			@QueryParam("end") @DefaultValue("50") Integer end) {
//		Set<Porudzbina> rezultat = porudzbinaService.vratiSvePorudzbine(start, end);
//		if (rezultat == null) {
//			throw new BadRequestException("Porudzbine za ovog korisnika ne postoje!");
//		}
//		return Response.ok(rezultat).build();
//	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response vratiSveKorisnikovePorudzbine(@QueryParam("start") @DefaultValue("0") Integer start,
			@QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Porudzbina> rezultat = porudzbinaService.vratiSvePorudzbineKorisnika(start, end);
		if(rezultat == null) {
			throw new BadRequestException("Porudzbine ovog korisnika ne postoje ili korisnik nije ulogovan");
		}
		return Response.ok(rezultat).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vratiPorudzbinu(@PathParam("id") Long id) {
		Porudzbina rezultat = porudzbinaService.vratiPorudzbinu(id);
		if(rezultat == null) {
			throw new BadRequestException(String.format("Porudzbina sa id-em: %d ne postoji!", id));
		}
		return Response.ok(rezultat).build();
	}
	
	@GET
	@Path("/{id}/proizvodi")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vratiProizvodeIzPorudzbine(@PathParam("id") Long id,@QueryParam("start") @DefaultValue("0") Integer start,
			@QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Proizvod> rezultat = porudzbinaService.vratiProizvodeIzPorudzbine(id, start, end);
		if(rezultat == null) {
			throw new BadRequestException("Proizvodi ne postoje u ovoj porudzbini ili je doslo do neke greske");
		}
		return Response.ok(rezultat).build();
	}
	
	@POST
	@Path("/nova")
	@Produces(MediaType.APPLICATION_JSON) 
	public Response napraviPorudzbinu() {
		Porudzbina rezultat = porudzbinaService.napraviPorudzbinu();
		if(rezultat == null) {
			throw new BadRequestException("Porudzbina nije napravljena kako treba, ili je korpa prazna");
		}
		return Response.ok(rezultat).build();
	}

}
