package lothar.hcs_services.rest.users;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lothar.hcs_services.model.Korpa;
import lothar.hcs_services.model.Proizvod;
import lothar.hcs_services.service.api.KorpaService;

@Path("users/korpa")
public class KorpaResource {

	@Inject
	private KorpaService korpaService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response dajKorpuKorisnika() {
		Korpa rezultat = korpaService.dajKorisnikovuKorpu();
		if (rezultat == null) {
			throw new BadRequestException("Korisnik nema korpu ili nema nikakvog sadrzaja u korpi");
		}
		return Response.ok(rezultat).build();
	}

	@POST
	@Path("/add/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dodajUkorpu(@PathParam("id") Long proizvodId,
			@QueryParam("kolicina") @DefaultValue("1") Integer kolicina) {
		Proizvod dodat = korpaService.dodajUkorpu(proizvodId, kolicina);
		if (dodat == null) {
			throw new BadRequestException("Proizvod koji ste pokusali da dodate ne postoji ili ga nema u zalihama");
		}
		return Response.ok(dodat).build();
	}

	@DELETE
	@Path("/remove/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response ukloniIzKorpe(@PathParam("id") Long proizvodId,
			@QueryParam("kolicina") Integer kolicina) {
		Proizvod rezultat = korpaService.ukloniIzKorpe(proizvodId,kolicina);
		if (rezultat == null) {
			throw new BadRequestException("Proizvod koji pokusavate da uklonite ne postoji ili nije u korpi!");
		}
		return Response.ok(rezultat).build();
	}

	@DELETE
	@Path("/remove/all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response ocistiKorpu() {
		Korpa rezultat = korpaService.ocistiKorpu();
		if (rezultat == null) {
			throw new BadRequestException("Rezultat je nekim cudom null provjeriti servisnu metodu");
		}
		return Response.ok(rezultat).build();
	}
}
