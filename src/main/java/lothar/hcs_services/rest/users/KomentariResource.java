	package lothar.hcs_services.rest.users;

import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lothar.hcs_services.model.Komentar;
import lothar.hcs_services.service.api.KomentarService;

@Path("users/komentari")
public class KomentariResource {
	
	@Inject private KomentarService komentarService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response sviKomentariKorisnika(@QueryParam("start") @DefaultValue("0") Integer start, @QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Komentar> rezultat = komentarService.sviKomentariKorisnika(start, end);
		if(rezultat == null || rezultat.isEmpty()) {
			throw new BadRequestException("Korisnik nije nista komentarisao ili ima neka greska");
		}
		return Response.ok(rezultat).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response vratiKomentarByID(@PathParam("id") Long komentarId) {
		Komentar rezultat = komentarService.vratiKomentarById(komentarId);
		if(rezultat == null) {
			throw new BadRequestException("Komentar sa datim id-em ne postoji ili niste unijeli dobre paramtere!");
		}
		return Response.ok(rezultat).build();
	}

	@GET
	@Path("/proizvod/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response sviKomentariProizvoda(@PathParam("id") Long proizvodId,
			@QueryParam("start") @DefaultValue("0") Integer start, @QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Komentar> rezultat = komentarService.sviKomentariProizvoda(proizvodId, start, end);
		if(rezultat == null || rezultat.isEmpty()) {
			throw new BadRequestException("Komentari za ovaj proizvod ne postoje ili ste unijeli pogresne parametre");
		}
		return Response.ok(rezultat).build();
	}
	
	@GET
	@Path("/proizvod/{id}/korisnik")
	@Produces(MediaType.APPLICATION_JSON)
	public Response sviKomentariKorisnikaProizvod(@PathParam("id") Long proizvodId,
			@QueryParam("start") @DefaultValue("0") Integer start, @QueryParam("end") @DefaultValue("50") Integer end) {
		Set<Komentar> rezultat = komentarService.sviKomentariProizvoda(proizvodId, start, end);
		if(rezultat == null || rezultat.isEmpty()) {
			throw new BadRequestException("Komentari za ovaj proizvod ili ovog korisnika ne postoje ili ste unijeli pogresne parametre");
		}
		return Response.ok(rezultat).build();
	}
	
//	-------------------------------------------------------------------------

	@POST
	@Path("/proizvod/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	public Response postaviKomentar(@PathParam("id") Long proizvodId, String tekst) { 
		if(tekst == null) {
			throw new BadRequestException("Ne mozete postaviti komentar bez teksta");
		}
		Komentar rezultat = komentarService.postaviKomentar(proizvodId,tekst);
		if(rezultat == null) {
			throw new BadRequestException("Komentar nije uspjesno postavljen,doslo je do neke greske");
		}
		return Response.ok(rezultat).build();
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response izbrisiKomentar(@PathParam("id") Long komentarId) {
		Komentar rezultat = komentarService.izbrisiKomentar(komentarId);
		if(rezultat == null) {
			throw new BadRequestException("Komentar koji pokusavate da izbrisete ne postoji ili nije dobar id koji ste unijeli");			
		}
		return Response.ok(rezultat).build();
	}
	
	@PUT
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	public Response izmijeniKomentar(@PathParam("id") Long komentarId,String tekst) {
		Komentar rezultat = komentarService.izmijeniKomentar(komentarId, tekst);
		if(rezultat == null) {
			throw new BadRequestException("Komentar koji pokusavate da izmijenite ne postoji ili nije vas pa ne mozete da ga mijenjate");		
		}
		return Response.ok(rezultat).build();
	}

}











